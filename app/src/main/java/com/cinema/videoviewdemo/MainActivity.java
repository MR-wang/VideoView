package com.cinema.videoviewdemo;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        MyVideoView2 vv= (MyVideoView2) findViewById(R.id.vv);
        vv.setUri(Uri.parse("http://cdn.ins.vcinema.cn/KONGBUJI/Tri/720PTHECURSED.mp4"));
        vv.start();
    }
}
